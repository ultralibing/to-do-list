const dateElement = document.getElementById('date');
const clear = document.querySelector('.clear');
const list = document.getElementById('list');
const input = document.querySelector('input');
const addButton = document.querySelector('.fa-plus-circle');

const CHECK = "fa-check-circle";
const UNCHECK = "fa-circle-thin";
const LINE_THROUGH = "lineThrough";
const CHANGE = "editable";

var status = "all", timer = null, delay = 260, click = 0;//double click

clear.addEventListener("click", function () {
    location.reload();
});

const options = { weekday: 'long', month: 'short', day: 'numeric' };
const today = new Date();
dateElement.innerHTML = today.toLocaleDateString("en-US", options);

let id = 0;

function addToDo(toDo, id, done, trash) {
    if (trash)
        return;
    const DONE = done ? CHECK : UNCHECK;
    const LINE = done ? LINE_THROUGH : "";

    const item = `<li class="item">
                    <i class="fa ${DONE} co" job="complete" id="${id}"></i>
                    <input disabled="disabled" class="text ${LINE}" job="edit" value="${toDo}" id="${id}" ">
                    <i class="fa fa-trash-o de" job="delete" id="${id}"></i>
                </li>
                `;
    const position = "beforeend";
    list.insertAdjacentHTML(position, item);
}

input.addEventListener("keyup", function (event) {
    if (event.keyCode === 13) {
        const toDo = input.value;
        if (toDo) {
            addToDo(toDo, id, false, false);
            id++;
            input.value = "";
        }
    }
});

addButton.addEventListener('click', function () {
    const toDo = input.value;
    if (toDo) {
        addToDo(toDo, id, false, false);
        id++;
        input.value = "";
    }
});

list.addEventListener("click", function (event) {
    const element = event.target;
    const elementJob = element.attributes.job.value;

    if (elementJob === 'complete') {
        completeToDo(element);
    } else if (elementJob === 'delete') {
        removeToDo(element);
    }
    else if (elementJob === 'edit') {
        editTodo(element);
    }
}, false);

function completeToDo(element) {
    element.classList.toggle(UNCHECK);
    element.classList.toggle(CHECK);
    element.parentNode.querySelector(".text").classList.toggle(LINE_THROUGH);

    if(status === "active"){
        element.parentNode.parentNode.removeChild(element.parentNode);
    }
    else if(status === "completed"){
        element.parentNode.parentNode.removeChild(element.parentNode);
    }
}

function removeToDo(element) {
    element.parentNode.parentNode.removeChild(element.parentNode);
}

function editTodo(element) {
    var newTodo, content = element.parentNode.querySelector(".text"), preText;
    click++;
    if (click === 1) {
        timer = setTimeout(function () {
            click = 0;
        }, (delay));
    } else {
        click = 0;
        clearTimeout(timer);
        preText = content.value;
        content.disabled = false;
        content.focus();
        content.onblur = () => {
            newTodo = content.value;
            content.disabled = true;
        };
    }

}