const CHECK = "fa-check-circle";
const UNCHECK = "fa-circle-thin";
const LINE_THROUGH = "lineThrough";
const CHANGE = "editable";
var status = "all", timer = null, delay = 260, click = 0;
$(document).ready(function(){
    $(".clear").click(function(){
        // localStorage.clear();
        location.reload();
    })
})
$(document).ready(function(){
    var options = { weekday: 'long', month: 'short', day: 'numeric' };
    var today = new Date();
    $("#date").html(today.toLocaleDateString("en-US", options));  
})

let id=0;
function addToDo(toDo, id, done, trash) {
    if (trash)
        return;
    const DONE = done ? CHECK : UNCHECK;
    const LINE = done ? LINE_THROUGH : "";

    const item = `<li class="item">
                    <i class="fa ${DONE} co" job="complete" id="${id}"></i>
                    <input disabled="disabled" class="text ${LINE}" job="edit" value="${toDo}" id="${id}" ">
                    <i class="fa fa-trash-o de" job="delete" id="${id}"></i>
                </li>
                `;
    $("#list").append(item);
}
$(document).ready(function(){
    
    $("#input").keyup(function(event){
        if (event.keyCode === 13) {
            var toDo = input.value;
            if (toDo) {
                addToDo(toDo, id, false, false);
                id++;
                input.value = "";
            }
        }
    })

    $("#list").click(function(event){
        var element = event.target;
        var elementJob = element.attributes.job.value;

        if (elementJob === 'complete') {
            completeToDo(element);
        } else if (elementJob === 'delete') {
            removeToDo(element);
        }
        else if (elementJob === 'edit') {
            editTodo(element);
        }
    })

    $(".fa-plus-circle").click(function() {
        const toDo = input.value;
        if (toDo) {
            addToDo(toDo, id, false, false);
            id++;
            input.value = "";
        }
    })

    function completeToDo(element) {
        element.classList.toggle(UNCHECK);
        element.classList.toggle(CHECK);
        element.parentNode.querySelector(".text").classList.toggle(LINE_THROUGH);
        if(status === "active"){
            element.parentNode.parentNode.removeChild(element.parentNode);
        }
        else if(status === "completed"){
            element.parentNode.parentNode.removeChild(element.parentNode);
        }
    }
    
    function removeToDo(element) {
        element.parentNode.parentNode.removeChild(element.parentNode);
    }
    
    function editTodo(element) {
        var newTodo, content = element.parentNode.querySelector(".text"), preText;
        click++;
        if (click === 1) {
            timer = setTimeout(function () {
                click = 0;
            }, (delay));
        } else {
            click = 0;
            clearTimeout(timer);
            preText = content.value;
            content.disabled = false;
            content.focus();
            content.onblur = () => {
                newTodo = content.value;
                content.disabled = true;
            };
        }
    
    }
})



